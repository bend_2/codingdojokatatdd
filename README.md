# Quick TDD setup with Node, ES6, Gulp and Mocha

This repository accompanies my blog post entitled "Quick TDD setup with Node, ES6, Gulp and Mocha."

You can view the blog post at
http://www.adamcowley.co.uk/javascript/quick-test-driven-development-setup-with-node-es6-and-gulp/

# Update Coding dojo Kata TDD FastIT
## Install Kata
```
 git clone `project`
 cd `project`
 npm i
```

## Structure Overview
Your code is located in `./src/*js`.

Your tests are located in `./tests/*js`.

## Run tests
Run `npm run tdd` to launch gulp watcher : each time .src/*js or .test/*js file are updated the watched re-run tests using `mocha`.


## ENJOY !!